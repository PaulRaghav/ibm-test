For task 4, I'm assuming that there should be a product quantity column in the cart_item table that indicates how much quantity of that item has been added to cart by the user. We then display an item only once in the cart and keep updating its quantity every time it's added to the cart. To do this, we follow these steps:

1. Update cart_item schema to have a product quantity item
2. When adding a record in the cart_item table, check if there's a record with that product_id already.
3. If true, increment the product quantity in that record.
4. Otherwise, insert into cart_item with product_id and quantity=1. 
